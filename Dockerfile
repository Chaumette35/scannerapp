FROM openjdk:17

EXPOSE 8082

WORKDIR /app

# Copie des fichiers de configuration Maven
COPY .mvn .mvn
COPY mvnw .
COPY mvnw.cmd .

# Copie de l'application
COPY pom.xml .
COPY src src

# Génère le .jar
RUN ./mvnw package -DskipTests


CMD ["java", "-jar", "target/scanner-0.0.1-SNAPSHOT.jar"]