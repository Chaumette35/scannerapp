### installer java 17
	 ubuntu : apt install openjdk-17-jdk openjdk-17-jre
	 mac : https://docs.oracle.com/en/java/javase/17/install/installation-jdk-macos.html#GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE
### vérifier en lançant 
	java -version
### cloner le dépôt
### aller dans le répertoire créé

Avant de lancer l'application, il faut créer une base de données postgreSQL à partir d'une image postgres. Il faut également préciser le port (5433:5432) ainsi que différents paramètres : 
 - POSTGRES_USER=db_user
 - POSTGRES_PASSWORD=db_password
 - POSTGRES_DB=db_scanner

### lancer ./mvnw spring-boot:run