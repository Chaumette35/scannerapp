package fr.ensai.scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.ensai.scanner.service.ScannerService;


@SpringBootApplication
public class StartApplication {

	@Autowired
	private static ScannerService scannerService;

	@Autowired
	@Qualifier("scannerService")
	private static ScannerService scannerService2;


	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
		if(StartApplication.scannerService == StartApplication.scannerService2){
		}
	}

}
