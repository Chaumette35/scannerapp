package fr.ensai.scanner.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.scanner.model.FolderModel;


@Repository
public interface FolderRepository extends CrudRepository<FolderModel, Long> {

    /**
     * Permet de récupérer le temps d'exécution moyen des dossiers
     * 
     * @return Temps d'exécution moyen
     */
    @Query("SELECT AVG(fo.time) FROM FolderModel fo")
    public Double averageExecTimeFolders();
    
}
