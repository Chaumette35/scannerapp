package fr.ensai.scanner.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.ensai.scanner.model.FileModel;


@Repository
public interface FileRepository extends CrudRepository<FileModel, Long> {

    /**
     * Permet de récupérer tous les fichiers d'un scan
     * 
     * @param id Identifiant d'un scan
     * @return Liste des fichiers du scan
     */
    @Query("""
        SELECT 
            fi
        FROM FileModel fi 
        JOIN fi.parentFolder fo
        WHERE fo.scanner.id = :id
        """)
    public List<FileModel> findFilesbyScannerId(@Param("id") long id);

    /**
     * Permet de récupérer le temps d'exécution moyen des fichiers
     * 
     * @return Temps d'exécution moyen
     */
    @Query("SELECT AVG(fi.time) FROM FileModel fi")
    public Double averageExecTimeFiles();
    
}
