package fr.ensai.scanner.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.scanner.model.Scanner;


@Repository
public interface ScannerRepository extends CrudRepository<Scanner, Long> {
    
}
