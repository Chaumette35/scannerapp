package fr.ensai.scanner.dto;

public class SearchInDto {
    public String search;
    public boolean caseSensitive;
    public String getSearch() {
        return search;
    }
    public boolean isCaseSensitive() {
        return caseSensitive;
    }
}
