package fr.ensai.scanner.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

/**
 * Représente un fichier à sauvegarder
 */
@Entity
@Table(name = "file")
public class FileModel {

    /**
     * Identifiant du fichier
     * A chaque sauvegarde, l'identifiant s'incrémente de 1
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private Long id;

    /**
     * Dossier parent du fichier
     * Cela permet d'effectuer une jointure entre les dossiers et les fichiers
     */
    @ManyToOne(optional = false)
    @JoinColumn(name = "parent_id")
    private FolderModel parentFolder;

    /**
     * Nom du fichier
     */
    @Column(name = "name")
    private String name;

    /**
     * Extension du fichier
     */
    @Column(name = "type")
    private String type;

    /**
     * Taille du fichier
     */
    @Column(name = "size")
    private long size;

    /**
     * Date de la dernière modification du fichier
     */
    @Column(name = "lastModified")
    private String lastModified;

    /**
     * Temps d'exécution de la sauvegarde du fichier
     */
    @Column(name = "exec_time")
    private Long time;

    public FileModel() {}

    /**
     * Construteur
     * @param id Identifiant du fichier
     * @param name Nom du fichier
     * @param type Extension du fichier
     * @param size Taille du fichier
     */
    public FileModel(Long id, String name, String type, long size) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FolderModel getParentFolder() {
        return parentFolder;
    }

    public void setFolder(FolderModel parentFolder) {
        this.parentFolder = parentFolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String toString() {
        String message = "File" + this.id + " : \n";
        message += "Name = " + this.name + " / ";
        message += "Type = " + this.type + " / ";
        message += "Size = " + this.size + " / ";
        message += "LastModified = " + this.lastModified + " / ";
        message += "ParentFolder = " + this.parentFolder.getName() + "\n";

        return message;
    }

    /**
     * Définit l'égalité entre deux fichiers
     * Deux fichiers sont égaux s'ils ont le même nom et le même dossier parent 
     * @param otherFile Fichier à comparer
     * @return True si les deux fichiers sont égaux et false sinon
     */
    public boolean equals(FileModel otherFile) {
        return (
            this.getName().equals(otherFile.getName()) &
            this.getParentFolder().getName().equals(otherFile.getParentFolder().getName())
        );
    }
    
}
