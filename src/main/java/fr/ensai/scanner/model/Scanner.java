package fr.ensai.scanner.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * Représente un scan à sauvegarder
 */
@Entity
@Table(name = "scanner")
public class Scanner {

    /**
     * Identifiant du scan
     * A chaque sauvegarde, l'identidiant s'incrémente de 1
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scanner_id")
    private Long id;

    /**
     * Nom du dossier de départ
     */
    @Column(name = "start_folder")
    private String startFolder;

    /**
     * Date du début du scan
     */
    @Column(name = "date")
    private String date;

    /**
     * Nom du filtre sur les noms des fichiers
     */
    @Column(name = "filterName")
    private String filterName;

    /**
     * Nom du filtre sur l'extension du fichier
     */
    @Column(name = "filterType")
    private String filterType;

    /**
     * Nombre maximal de fichier à sauvegarder
     */
    @Column(name = "maxFile")
    private Integer maxFile;

    /**
     * Profondeur maximale dans l'arborescence des dossiers
     */
    @Column(name = "maxDepth")
    private Integer maxDepth;

    /**
     * Temps d'exécution du scan
     */
    @Column(name = "exec_time")
    private Long time;

    /**
     * Méthode de scan, local ou aws
     */
    @Column(name = "fileSystem")
    private String fileSystem;

    public Scanner() {}

    /**
     * Constructeur
     * 
     * @param id Identifiant du scan
     * @param time Temps d'exécution du scan
     */
    public Scanner(Long id, Long time) {
        this.id = id;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartFolder() {
        return startFolder;
    }

    public void setStartFolder(String startFolder) {
        this.startFolder = startFolder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public Integer getMaxFile() {
        return maxFile;
    }

    public void setMaxFile(Integer maxFile) {
        this.maxFile = maxFile;
    }

    public void dicrMaxFile() {
        this.maxFile -= 1;
    }

    public Integer getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getFileSystem() {
        return fileSystem;
    }

    public void setFileSystem(String fileSystem) {
        this.fileSystem = fileSystem;
    }
    
}
