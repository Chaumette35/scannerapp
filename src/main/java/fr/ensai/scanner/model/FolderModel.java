package fr.ensai.scanner.model;

import fr.ensai.scanner.service.composite.FolderComposite;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * Représente un dossier à sauvegarder
 */
@Entity
@Table(name = "folder")
public class FolderModel {

    /**
     * Identifiant du dossier
     * A chaque sauvegarde, l'identifiant s'incrémente de 1
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "folder_id")
    private Long id;

    /**
     * Scan du dossier parent
     * Cela permet d'effectuer une jointure entre les scans et les dossiers
     */
    @ManyToOne(optional = false)
    @JoinColumn(name = "scanner_id")
    private Scanner scanner;

    /**
     * Dossier parent du dossier
     * Cela permet d'effectuer une jointure entre les scans et les dossiers
     */
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private FolderModel parentFolder;

    /**
     * Nom du dossier
     */
    @Column(name = "name")
    private String name;

    /**
     * Type du dossier
     * Ici, folder
     */
    @Column(name = "type")
    private String type;

    /**
     * Taille du dossier
     * Correspond à la somme des tailles des éléments dans le dossier
     */
    @Column(name = "size")
    private Long size;

    /**
     * Dernière date de modification du dossier
     */
    @Column(name = "lastModified")
    private String lastModified;

    /**
     * Temps d'exécution de la sauvegarde du dossier
     */
    @Column(name = "exec_time")
    private Long time;

    /**
     * Profondeur maximale dans l'arborescence des dossiers
     */
    @Transient
    private Integer maxDepth;

    public FolderModel() {}

    /**
     * Constructeur
     * 
     * @param id Identifiant du dossier
     * @param name Nom du dossier
     * @param type Extension du dossier
     * @param size Taille du dossier
     */
    public FolderModel(Long id, String name, String type, Long size) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public FolderModel getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(FolderModel parentFolder) {
        this.parentFolder = parentFolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Integer getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    /** 
     * Convertit le dossier en un dossier utilisable pour la composition
     * 
     * @return Dossier utilisable pour la composition
     */
    public FolderComposite convert() {
        FolderComposite returnedFolder = new FolderComposite(this.name, this.scanner);
        return returnedFolder;
    }
    
}
