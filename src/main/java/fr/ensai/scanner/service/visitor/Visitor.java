package fr.ensai.scanner.service.visitor;

import fr.ensai.scanner.service.composite.AWS;
import fr.ensai.scanner.service.composite.FileLeaf;
import fr.ensai.scanner.service.composite.FolderComposite;

/**
 * Interface pour visiter différents objets
 */
public interface Visitor {

    public abstract void visitFile(FileLeaf file);

    public abstract void visitFolder(FolderComposite folder);

    public abstract void visitBucket(AWS aws);
    
}
