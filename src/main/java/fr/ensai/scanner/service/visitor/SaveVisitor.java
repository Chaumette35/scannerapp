package fr.ensai.scanner.service.visitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.FolderModel;
import fr.ensai.scanner.service.FileService;
import fr.ensai.scanner.service.FolderService;
import fr.ensai.scanner.service.composite.AWS;
import fr.ensai.scanner.service.composite.FileLeaf;
import fr.ensai.scanner.service.composite.FileSystemComponent;
import fr.ensai.scanner.service.composite.FolderComposite;

/**
 * Classe visiteur pour sauvegarder les dossiers/fichiers
 */
@Component
public class SaveVisitor implements Visitor {

    /**
     * Classe pour sauvegarder les dossiers
     */
    @Autowired
    FolderService folderService;

    /**
     * Classe pour sauvegarder les fichiers
     */
    @Autowired
    FileService fileService;

    /**
     * Permet de définir de nouvelles variables
     */
    @Autowired
    ApplicationContext context;

    /**
     * Dossier parent afin du fichier/dossier à sauvegarder
     */
    private FolderModel parentFolder;

    public SaveVisitor() {}

    /**
     * Constructeur
     * 
     * @param parentFolder Dossier parent
     */
    public SaveVisitor(FolderModel parentFolder) {
        this.parentFolder = parentFolder;
    }

    public FolderModel getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(FolderModel parentFolder) {
        this.parentFolder = parentFolder;
    }

    /**
     * Permet de sauvegarder un fichier
     * 
     * @param file Fichier à analyser pour voir si le il peut être sauvegarder
     */
    @Override
    public void visitFile(FileLeaf file) {
        long start = System.currentTimeMillis(); // Au début de la sauvegarde, on récupère l'heure pour le temps d'exécution

        String filterName = this.parentFolder.getScanner().getFilterName();
        String filterType = this.parentFolder.getScanner().getFilterType();
        Integer maxFile = this.parentFolder.getScanner().getMaxFile();
        boolean canSave = true;

        if (filterName != null) {
            // On regarde si le nom du fichier ne contient pas le nom du filtre
            if (!file.getName().toLowerCase().contains(filterName)) {
                canSave = false;
            }
        } 
        if (filterType != null) {
            // On regarde si l'extension du fichier correspond au filtre
            if (!file.getType().equals(filterType)) {
                canSave = false;
            }
        }
        if (maxFile != null) {
            // On regarde si l'on peut encore sauvegarder des fichiers
            if (maxFile <= 0) {
                canSave = false;
            }
        }

        if (canSave) {
            FileModel convertedFile = file.convert();
            FileModel savedFile = fileService.saveFile(convertedFile, this.parentFolder);

            if (maxFile != null) {
                // Après avoir sauvegarder un fichier, on décrémente le nombre maximal de fichiers
                this.parentFolder.getScanner().dicrMaxFile();
            }
            long end = System.currentTimeMillis();
            // A la fin, on modifie le fichier en rajoutant le temps d'exécution
            fileService.modifFileTime(savedFile, end - start);
        }
    }

    /**
     * Permet de sauvegarder un dossier
     * 
     * @param folder Dossier à analyser pour voir si l'on peut le sauvegarder
     */
    @Override
    public void visitFolder(FolderComposite folder) {
        long start = System.currentTimeMillis(); // Au début de la sauvegarde, on récupère l'heure pour le temps d'exécution

        // On récupère la profondeur maximale
        Integer maxDepth = folder.getScanner().getMaxDepth();
        boolean canSave = true;

        if (maxDepth != null) {
            // Cette valeur a potentiellement été changée, il faut donc la mettre à jour
            maxDepth = (this.parentFolder == null) 
            ? maxDepth 
            : (
                this.parentFolder.getMaxDepth() != null
                ? this.parentFolder.getMaxDepth()
                : maxDepth
            );

            if (maxDepth <= 0) {
                canSave = false;
            }
        }

        if (canSave) {
            FolderModel convertedFolder = folder.convert();
            FolderModel parentFolder = folderService.saveFolder(convertedFolder, this.parentFolder);

            if (maxDepth != null) {
                // Après avoir sauvegarder un dossier, on diminue la valeur de la profondeur
                parentFolder.setMaxDepth(maxDepth - 1);
            }

            for (FileSystemComponent file: folder.getFiles()) {
                // On parcourt l'ensemble des fichiers/dossiers
                // On crée un nouveau visiteur avec le précédent dossier parent
                SaveVisitor newVisitor = context.getBean(SaveVisitor.class);
                newVisitor.setParentFolder(parentFolder);

                // On lance une nouvelle sauvegarde avec les nouveaux fichiers/dossiers
                file.accept(newVisitor);
            }

            long end = System.currentTimeMillis();
            // A la fin, on modifie le dossier en rajoutant le temps d'exécution
            folderService.modifFolderTime(parentFolder, end - start);
        }
    }

    /**
     * Permet de sauvegarder les éléments dans un bucket S3
     * 
     * @param aws Lecteur d'un bucket S3
     */
    @Override
    public void visitBucket(AWS aws) {
        // Tout d'abord, il faut créer une requête pour lire les dossiers/fichiers
        ListObjectsV2Request request;
        if (this.parentFolder == null) { // Cela correspond au cas de départ avec le dossier de départ
            request = new ListObjectsV2Request()
                .withBucketName(aws.getBucketName())
                .withPrefix(aws.getPrefix())
                .withContinuationToken(aws.getContinuationToken())
                .withMaxKeys(1); // On lit donc seulement le dossier de départ
        } else { // Pour les autres fichiers, on lit tous les fichiers/dossiers
            request  = new ListObjectsV2Request()
                .withBucketName(aws.getBucketName())
                .withPrefix(aws.getPrefix())
                .withContinuationToken(aws.getContinuationToken());
        }

        // On récupère le résultat de la requête
        ListObjectsV2Result result = aws.s3.listObjectsV2(request);

        // On parcourt tous les fichiers/dossiers de notre requête
        for (S3ObjectSummary object: result.getObjectSummaries()) {
            // On regarde si le fichier/dossier n'a pas déjà été lu
            if (!aws.getFiles().contains(object.getKey())) {
                String[] namesObject = object.getKey().split("/");
                // Les dossiers finissent tous par "/"
                if (object.getKey().endsWith("/")) {
                    Integer maxDepth = aws.getScanner().getMaxDepth();
                    boolean canSave = true;

                    if (maxDepth != null) {
                        // Comme pour un dossier classique, on met à jour la valeur de la profondeur de la profondeur maximale
                        maxDepth = (this.parentFolder == null) 
                        ? maxDepth 
                        : (
                            this.parentFolder.getMaxDepth() != null
                            ? this.parentFolder.getMaxDepth()
                            : maxDepth
                        );
                        // On regarde si l'on est pas allé trop loin dans l'exploration
                        if (maxDepth <= 0) {
                            canSave = false;
                        }
                    }

                    // On ajoute ce dossier à la liste pour ne plus le relire plus tard
                    aws.addFiles(object.getKey());
                    if (canSave) {
                        // On crée un dossier sauvegardable
                        FolderModel folder = new FolderModel();
                        folder.setName(namesObject[namesObject.length - 1]);
                        folder.setType("AWS folder");
                        folder.setLastModified(aws.dateFormat.format(object.getLastModified()).toString());
                        folder.setSize(object.getSize());
                        folder.setScanner(aws.getScanner());

                        // Condition pour éviter certains problèmes avec les dossiers parents
                        if (aws.getPrefix() == null) {
                            this.parentFolder = null;
                        }
                        FolderModel parentFolder = folderService.saveFolder(folder, this.parentFolder);

                        // On crée un nouveau lecteur de bucket S3 où 
                        // le préfixe devient le nom du dossier afin de lire seulement les éléments à l'intérieur
                        AWS newAWS = new AWS(aws.getBucketName(), object.getKey(), aws.getScanner());
                        // On met à jour le token de continuation pour ne pas relire les mêmes dossiers/fichiers
                        newAWS.setContinuationToken(result.getNextContinuationToken());
                        newAWS.setFiles(aws.getFiles());

                        if (maxDepth != null) {
                            // On décrémente la profondeur de 1
                            parentFolder.setMaxDepth(maxDepth - 1);
                        }

                        // Comme pour les dossiers, on crée un nouveau visiteur pour garder en mémoire le dossier parent
                        SaveVisitor newVisitor = context.getBean(SaveVisitor.class);
                        newVisitor.setParentFolder(parentFolder);
                        // On continue la sauvegarde des éléments du bucket
                        newAWS.accept(newVisitor);
                    }
                } else { // Si l'élément n'est pas un dossier alors il s'agit d'un fichier
                    String[] types = object.getKey().split("\\.");

                    String filterName = this.parentFolder.getScanner().getFilterName();
                    String filterType = this.parentFolder.getScanner().getFilterType();
                    Integer maxFile = this.parentFolder.getScanner().getMaxFile();
                    Integer maxDepth = this.parentFolder.getScanner().getMaxDepth();
                    boolean canSave = true;

                    // Comme pour les fichiers en local, on effectue différents filtres sur le fichier dans le bucket
                    if (filterName != null) {
                        if (!namesObject[namesObject.length - 1].toLowerCase().contains(filterName)) {
                            canSave = false;
                        }
                    } 
                    if (filterType != null) {
                        if (!types[types.length - 1].equals(filterType)) {
                            canSave = false;
                        }
                    }
                    if (maxFile != null) {
                        if (maxFile <= 0) {
                            canSave = false;
                        }
                    }
                    if (maxDepth != null) {
                        // On doit également vérifier la profondeur sinon on perd la notion d'arborescence
                        if (namesObject.length > maxDepth + 1) {
                            canSave = false;
                        }
                    }

                    if (canSave) {
                        // On crée un fichier sauvegardable
                        FileModel file = new FileModel();
                        file.setName(namesObject[namesObject.length - 1]);
                        file.setSize(object.getSize());
                        file.setLastModified(aws.dateFormat.format(object.getLastModified()).toString());
                        file.setType(types[types.length - 1]);

                        fileService.saveFile(file, this.parentFolder);

                        if (maxFile != null) {
                            // On diminue le nombre de fichiers sauvegardables
                            this.parentFolder.getScanner().dicrMaxFile();
                        }
                    }

                    // On ajoute ce fichier pour ne pas le relire
                    aws.addFiles(object.getKey());
                }
            }
        }
    }
    
}
