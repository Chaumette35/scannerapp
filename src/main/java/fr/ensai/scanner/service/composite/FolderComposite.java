package fr.ensai.scanner.service.composite;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ensai.scanner.model.FolderModel;
import fr.ensai.scanner.model.Scanner;
import fr.ensai.scanner.service.visitor.Visitor;


public class FolderComposite implements FileSystemComponent {

    /**
     * Chemin vers le dossier en local
     */
    private String path;

    /**
     * Dossier de classe File
     * Permet d'obtenir des informations sur les dossiers
     */
    private File fileObject;

    /**
     * Scan en cours
     */
    private Scanner scanner;

    /**
     * Liste des fichiers/dossiers dans ce dossier
     */
    private List<FileSystemComponent> files = new ArrayList<>();

    /**
     * Constructeur
     * 
     * @param path Chemin vers le dossier en local
     * @param scanner Scan en cours
     */
    public FolderComposite(String path, Scanner scanner) {
        this.path = path;
        this.fileObject = new File(path);
        this.scanner = scanner;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public List<FileSystemComponent> getFiles() {
        List<FileSystemComponent> result = new ArrayList<>();
        for (File file: this.fileObject.listFiles()) {
            if (file.isDirectory()) {
                result.add(this.convert(file));
            } else {
                result.add(new FileLeaf(file.getAbsolutePath()));
            }
        }
        return result;
    }

    public void addFiles(FileSystemComponent file) {
        this.files.add(file);
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return this.fileObject.getName();
    }

    public String getType() {
        return "Local folder";
    }

    @Override
    public long getSize() {
        long result = 0;
        for (FileSystemComponent file: this.getFiles()) {
            result += file.getSize();
        }
        return result;
    }

    public String getLastModified() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date(this.fileObject.lastModified())).toString();
    }

    /**
     * Convertit un dossier de classe File
     * 
     * @return Fichier ou dossier pour la composition
     */
    @Override
    public FileSystemComponent convert(File file) {
        return new FolderComposite(file.getAbsolutePath(), this.scanner);
    }

    /**
     * Convertit le dossier afin de le sauvegarder
     * 
     * @return Dossier à sauvegarder
     */
    public FolderModel convert() {
        FolderModel returnedFolder = new FolderModel();
        returnedFolder.setName(this.getName());
        returnedFolder.setType(this.getType());
        returnedFolder.setSize(this.getSize());
        returnedFolder.setLastModified(this.getLastModified());
        returnedFolder.setScanner(this.scanner);

        return returnedFolder;
    }

    /**
     * Permet d'être visiter
     * 
     * @param v Visiteur
     */
    @Override
    public void accept(Visitor v) {
        v.visitFolder(this);
    }
    
}
