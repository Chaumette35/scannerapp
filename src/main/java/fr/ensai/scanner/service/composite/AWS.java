package fr.ensai.scanner.service.composite;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import fr.ensai.scanner.model.Scanner;
import fr.ensai.scanner.service.visitor.Visitor;

/**
 * Représente un lecteur d'un bucket S3
 */
public class AWS {

    /**
     * Client AWS pour accéder aux buckets S3
     */
    public final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();

    /**
     * Format de date simple
     */
    public final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * Set de l'ensemble des fichiers/dossiers lus par le scan
     */
    private Set<String> files = new HashSet<>();

    /**
     * Nom du bucket S3
     */
    private String bucketName;

    /**
     * Scan en cours
     */
    private Scanner scanner;

    /**
     * Préfixe de départ du bucket
     * Permet de restreindre les fichiers/dossiers à lire
     */
    private String prefix;

    /**
     * Token de continuation
     * Permet de ne pas lire deux fois un même fichier/dossier
     */
    private String continuationToken;

    /**
     * Constructeur
     * 
     * @param bucketName Nom du bucket S3
     * @param prefix Préfixe de départ du bucket
     * @param scanner Scan en cours
     */
    public AWS(String bucketName, String prefix, Scanner scanner) {
        this.bucketName = bucketName;
        this.prefix = prefix;
        this.scanner = scanner;
    }

    public Set<String> getFiles() {
        return files;
    }

    public void addFiles(String file) {
        this.files.add(file);
    }

    public void setFiles(Set<String> files) {
        this.files = files;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getContinuationToken() {
        return continuationToken;
    }

    public void setContinuationToken(String continuationToken) {
        this.continuationToken = continuationToken;
    }

    /**
     * Permet d'être visiter
     * 
     * @param v Visiteur
     */
    public void accept(Visitor v) {
        v.visitBucket(this);
    }
    
}
