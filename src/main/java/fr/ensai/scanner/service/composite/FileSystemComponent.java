package fr.ensai.scanner.service.composite;

import java.io.File;

import fr.ensai.scanner.service.visitor.Visitor;

/**
 * Interface pour représenter la composition de fichiers et de dossiers
 */
public interface FileSystemComponent {

    public abstract long getSize();

    public abstract FileSystemComponent convert(File file);

    public abstract void accept(Visitor v);
    
}
