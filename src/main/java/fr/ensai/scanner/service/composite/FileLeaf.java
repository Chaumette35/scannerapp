package fr.ensai.scanner.service.composite;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.service.visitor.Visitor;

/**
 * Représente un fichier et correspond à une feuille dans la composition
 */
public class FileLeaf implements FileSystemComponent {

    /**
     * Chemin vers le fichier en local
     */
    private String path;

    /**
     * Fichier de classe File
     * Permet d'obtenir des informations sur les fichiers
     */
    private File fileObject;

    /**
     * Constructeur
     * @param path Chemin vers le fichier en local
     */
    public FileLeaf(String path) {
        this.path = path;
        this.fileObject = new File(path);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return this.fileObject.getName();
    }

    public String getType() {
        String[] parts = this.fileObject.getName().split("\\.");
        return parts[parts.length - 1];
    }

    @Override
    public long getSize() {
        return this.fileObject.length();
    }

    public String getLastModified() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date(this.fileObject.lastModified())).toString();
    }

    /**
     * Convertit un fichier de classe File
     * 
     * @return Fichier ou dossier pour la composition
     */
    @Override
    public FileSystemComponent convert(File file) {
        return new FileLeaf(file.getAbsolutePath());
    }

    /**
     * Convertit le fichier afin de le sauvegarder
     * 
     * @return Fichier à sauvegarder
     */
    public FileModel convert() {
        FileModel returnedFile = new FileModel();
        returnedFile.setName(this.getName());
        returnedFile.setType(this.getType());
        returnedFile.setSize(this.getSize());
        returnedFile.setLastModified(this.getLastModified());

        return returnedFile;
    }

    /**
     * Permet d'être visiter
     * 
     * @param v Visiteur
     */
    @Override
    public void accept(Visitor v) {
        v.visitFile(this);
    }
    
}
