package fr.ensai.scanner.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.scanner.model.FolderModel;
import fr.ensai.scanner.repository.FolderRepository;


@Service
public class FolderService {

    Logger LOG  = LoggerFactory.getLogger(FolderService.class);

    @Autowired
    FolderRepository folderRepository;

    /**
     * Permet de récupérer un dossier selon son identifiant
     * 
     * @param id Identifiant du dossier récupéré
     * @return Dossier récupéré
     */
    public Optional<FolderModel> getFolder(final long id) {
        return folderRepository.findById(id);
    }

    /**
     * Permet de récupérer tous les dossiers
     * 
     * @return Ensemble des dossiers récupérés
     */
    public Iterable<FolderModel> getFolders() {
        return folderRepository.findAll();
    }

    /**
     * Permet de sauvegarder un dossier
     * 
     * @param folder Dossier à sauvegarder
     * @param parentFolder Dossier parent du dossier
     * @return Dossier sauvegardé
     */
    public FolderModel saveFolder(FolderModel folder, FolderModel parentFolder) {
        if (parentFolder != null) {
            folder.setParentFolder(parentFolder);
        }

        @SuppressWarnings("null")
        FolderModel savedFolder = folderRepository.save(folder);

        return savedFolder;
    }

    public FolderModel modifFolderTime(FolderModel folder, long time) {
        folder.setTime(time);

        FolderModel savedFolder = folderRepository.save(folder);

        return savedFolder;
    }

    /**
     * Permet de supprimer un dossier selon son identifiant
     * 
     * @param id Identifiant du dossier à supprimer
     * @return True si le dossier a été supprimer et false sinon
     */
    public boolean deleteFolder(final long id) {
        Optional<FolderModel> folder = this.getFolder(id);
        folderRepository.deleteById(id);

        return folder.isPresent();
    }

    /**
     * Permet de récupérer le temps d'exécution moyen des dossiers
     * 
     * @return Temps d'exécution moyen des dossiers
     */
    public Double avgExecTimeFolders() {
        return folderRepository.averageExecTimeFolders();
    }
    
}
