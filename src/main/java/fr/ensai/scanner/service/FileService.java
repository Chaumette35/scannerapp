package fr.ensai.scanner.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.FolderModel;
import fr.ensai.scanner.repository.FileRepository;


@Service
public class FileService {

    Logger LOG = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private FileRepository fileRepository;

    /**
     * Permet de récupérer un fichier selon son identifiant
     * 
     * @param id Identifiant du fichier
     * @return Fichier récupéré
     */
    public Optional<FileModel> getFile(final long id) {
        return fileRepository.findById(id);
    }

    /**
     * Permet de récupérer tous les les fichiers
     * 
     * @return Ensemble des fichiers récupérés
     */
    public Iterable<FileModel> getFiles() {
        return fileRepository.findAll();
    }

    /**
     * Permet de sauvegarder un fichier
     * 
     * @param file Fichier à sauvegarder
     * @param parentFolder Dossier parent du fichier
     * @return Fichier enregistré
     */
    public FileModel saveFile(FileModel file, FolderModel parentFolder) {
        file.setFolder(parentFolder);

        FileModel savedFile = fileRepository.save(file);

        return savedFile;
    }

    public FileModel modifFileTime(FileModel file, long time) {
        file.setTime(time);

        FileModel savedFile = fileRepository.save(file);

        return savedFile;
    }

    /**
     * Permet de récupérer tous les fichiers d'un scan
     * 
     * @param id Identifiant d'un scan
     * @return Liste des fichiers d'un scan
     */
    public List<FileModel> getFilesByScannerId(long id) {
        return fileRepository.findFilesbyScannerId(id);
    }

    /**
     * Permet de supprimer un fichier selon son identifiant
     * 
     * @param id Identifiant du fichier à supprimer
     * @return True si le dossier a été supprimer et false sinon
     */
    public boolean deleteFile(final long id) {
        Optional<FileModel> file = this.getFile(id);
        fileRepository.deleteById(id);

        return file.isPresent();
    }

    /**
     * Permet de récupérer le temps d'exécution moyen des fichiers
     * 
     * @return Temps d'exécution moyen des fichiers
     */
    public Double avgExecTimeFiles() {
        return fileRepository.averageExecTimeFiles();
    }
    
}
