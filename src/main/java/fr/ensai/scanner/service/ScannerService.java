package fr.ensai.scanner.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.Scanner;
import fr.ensai.scanner.repository.ScannerRepository;
import fr.ensai.scanner.service.composite.AWS;
import fr.ensai.scanner.service.composite.FolderComposite;
import fr.ensai.scanner.service.visitor.SaveVisitor;


@Service
public class ScannerService {

    @Autowired
    ScannerRepository scannerRepository;

    @Autowired
    FolderService folderService;

    @Autowired
    FileService fileService;

    @Autowired
    ApplicationContext context;

    /**
     * Permet de récupérer un scan selon son identifiant
     * 
     * @param id Identifiant du scan
     * @return Scan récupéré
     */
    public Optional<Scanner> getScanner(final long id) {
        return scannerRepository.findById(id);
    }

    /**
     * Permet de récupérer tous les scans effectués
     * 
     * @return Ensemble des scans récupérés
     */
    public Iterable<Scanner> getScanners() {
        return scannerRepository.findAll();
    }

    /**
     * Permet de sauvegarder un scan local
     * 
     * @param scanner Scan à sauvegarder
     * @param startFolder Chemin du dossier de départ
     * @return
     */
    public Scanner saveScanner(Scanner scanner, String startFolder) {
        long start = System.currentTimeMillis();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        scanner.setDate(dateFormat.format(new Date(start)));

        Scanner savedScanner = scannerRepository.save(scanner);

        FolderComposite startFolderCompo = new FolderComposite(startFolder, scanner);
        SaveVisitor saveVisitor = context.getBean(SaveVisitor.class);
        saveVisitor.setParentFolder(null);
        startFolderCompo.accept(saveVisitor);

        long end = System.currentTimeMillis();

        Scanner savedScannerTime = this.modifScannerTime(savedScanner, end - start);
        return savedScannerTime;
    }

    /**
     * Permet de sauvegarder un scan sur AWS
     * 
     * @param scanner Scan à sauvegarder
     * @param bucketName Nom du bucket S3
     * @return Scan sauvegardé
     */
    public Scanner saveBucket(Scanner scanner, String bucketName) {
        long start = System.currentTimeMillis();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        scanner.setDate(dateFormat.format(new Date(start)));

        Scanner savedScanner = scannerRepository.save(scanner);

        AWS aws = new AWS(bucketName, scanner.getStartFolder(), scanner);
        SaveVisitor saveVisitor = context.getBean(SaveVisitor.class);
        saveVisitor.setParentFolder(null);
        aws.accept(saveVisitor);

        long end = System.currentTimeMillis();

        Scanner savedScannerTime = this.modifScannerTime(savedScanner, end - start);
        return savedScannerTime;
    }

    /**
     * Permet de modifier un scan pour y intégrer le temps d'exécution
     * 
     * @param scanner Scan à modifier
     * @param time temps d'exécution
     * @return Scan modifié
     */
    public Scanner modifScannerTime(Scanner scanner, long time) {
        scanner.setTime(time);

        Scanner savedScanner = scannerRepository.save(scanner);

        return savedScanner;
    }

    /**
     * Permet de récupérer les fichiers d'un scan
     * @param id Identifiant d'un scan
     * @return Liste des fichiers d'un scan
     */
    public List<FileModel> getFilesById(long id) {
        return fileService.getFilesByScannerId(id);
    }

    /**
     * Permet de supprimer un scan selon son identifiant
     * 
     * @param id Identifiant d'un scan à spprimer
     * @return True si le scan a été supprimé et false sinon
     */
    public boolean deleteScanner(final long id) {
        Optional<Scanner> scanner = this.getScanner(id);
        scannerRepository.deleteById(id);

        return scanner.isPresent();
    }

    /**
     * Permet de récupérer le temps d'exécution moyen des fichiers ou des dossiers en fonction d'un paramètre
     * 
     * @param fileOrFolder Composant dont on souhaite connaitre le temps d'exécution. Soit file, soit folder
     * @return Temps d'exécution moyen des fichiers ou des dossiers
     */
    public Double avgExecTime(String fileOrFolder) {
        if (fileOrFolder.equals("file")) {
            return fileService.avgExecTimeFiles();
        } else if (fileOrFolder.equals("folder")) {
            return folderService.avgExecTimeFolders();
        } else {
            return null;
        }
    }

}
