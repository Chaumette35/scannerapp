package fr.ensai.scanner.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ensai.scanner.model.FileModel;

/**
 * Classe contenant des fonctions utiles pour les scans
 */
public class Utils {

    /**
     * Fonction pour récupérer les fichiers en communs entre deux listes de fichiers
     * 
     * @param files1 Première liste de fichiers à comparer
     * @param files2 Seconde liste de fichiers à comparer
     * @return Liste des fichiers en communs
     */
    public static List<FileModel> sameElements(List<FileModel> files1, List<FileModel> files2) {
        List<FileModel> result = new ArrayList<>();

        for (FileModel file1: files1) {
            for (FileModel file2: files2) {
                if (file1.equals(file2)) {
                    result.add(file1);
                }
            }
        }
        return result;
    }

    /**
     * Fonction pour récupérer les fichiers présents dans le premier fichier mais pas dans le second fichier
     * 
     * @param files1 Première liste de fichiers à comparer
     * @param files2 Seconde liste de fichiers à comparer
     * @return Liste des fichiers seulement dans la première liste
     */
    public static List<FileModel> diffElements(List<FileModel> files1, List<FileModel> files2) {
        List<FileModel> result = new ArrayList<>();

        for (FileModel file1: files1) {
            boolean equal = false;
            for (FileModel file2: files2) {
                if (file1.equals(file2)) {
                    equal = true;
                }
            }
            if (!equal) {
                result.add(file1);
            }
        }
        return result;
    }

    /**
     * Fonction regroupeant les deux précédentes afin d'obtenir les fichiers en communs, 
     * les fichiers présents seulement dans la première liste et les fichiers présents seulement dans la seconde liste
     * 
     * @param files1 Première liste de fichiers à comparer
     * @param files2 Seconde liste de fichiers à comparer
     * @return Map avec les trois listes d'intérêt
     */
    public static Map<String, List<FileModel>> compareElements(List<FileModel> files1, List<FileModel> files2) {
        Map<String, List<FileModel>> result = new HashMap<>();

        result.put("same", Utils.sameElements(files1, files2));
        result.put("diff1", Utils.diffElements(files1, files2));
        result.put("diff2", Utils.diffElements(files2, files1));

        return result;
    }
    
}
