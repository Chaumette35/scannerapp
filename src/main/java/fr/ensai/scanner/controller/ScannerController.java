package fr.ensai.scanner.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.Scanner;
import fr.ensai.scanner.service.ScannerService;
import fr.ensai.scanner.service.Utils;


@RestController
public class ScannerController {

    @Autowired
    private ScannerService scannerService;

    /**
     * Permet d'obtenir tous les scans effectués
     * 
     * @return Liste des scans effectués
     */
    @GetMapping("/scanners")
    public Iterable<Scanner> getScanners() {
        return scannerService.getScanners();
    }

    /**
     * Permet de dupliquer un scan effectué en changeant les filtres
     * 
     * @param id Identifiant du scan à dupliquer
     * @param filterName Nom du filtre sur les noms des fichiers
     * @param filterType Nom du filtre sur les extensions des fichiers
     * @param maxFile Nombre maximal de fichiers à sauvegarder
     * @param maxDepth Profondeur maximale dans l'arborescence des dossiers
     * @return Scan dupliqué
     */
    @GetMapping("/scanners/duplicate/{id}")
    public Scanner duplicateScanner(
        @PathVariable long id, 
        @RequestParam(required = false) String filterName, 
        @RequestParam(required = false) String filterType, 
        @RequestParam(required = false) Integer maxFile, 
        @RequestParam(required = false) Integer maxDepth
    ) {
        Optional<Scanner> result = scannerService.getScanner(id);
        Scanner duplicatedScanner;
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            Scanner scanner = new Scanner();

            // On change les paramètres du scan d'origine
            scanner.setStartFolder(result.get().getStartFolder());
            scanner.setFilterName(filterName);
            scanner.setFilterType(filterType);
            scanner.setMaxFile(maxFile);
            scanner.setMaxDepth(maxDepth);
            scanner.setFileSystem(result.get().getFileSystem());

            // Puis, on joue le scan en fonction du la méthode de scan
            if (result.get().getFileSystem() == "local") {
                duplicatedScanner = scannerService.saveScanner(scanner, scanner.getStartFolder());
            } else {
                String[] fileSystem = result.get().getFileSystem().split("\\/"); // Cela permet de récupérer le nom du bucket S3
                duplicatedScanner = scannerService.saveBucket(scanner, fileSystem[1]);
            }
        }
        return duplicatedScanner;
    }

    /**
     * Permet de rejouer un scan avec les mêmes filtres que le scan d'origine
     * 
     * @param id Identifiant du scan à dupliquer
     * @return Scan rejoué
     */
    @GetMapping("/scanners/replay/{id}")
    public Scanner replayScanner(@PathVariable long id) {
        Optional<Scanner> result = scannerService.getScanner(id);
        Scanner replayedScanner;
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            Scanner scanner = new Scanner();

            // On récupère tous les paramètres du scan d'origine
            scanner.setStartFolder(result.get().getStartFolder());
            scanner.setFilterName(result.get().getFilterName());
            scanner.setFilterType(result.get().getFilterType());
            scanner.setMaxFile(result.get().getMaxFile());
            scanner.setMaxDepth(result.get().getMaxDepth());
            scanner.setFileSystem(result.get().getFileSystem());

            // Puis, on joue le scan en fonction du la méthode de scan
            if (result.get().getFileSystem() == "local") {
                replayedScanner = scannerService.saveScanner(scanner, scanner.getStartFolder());
            } else {
                String[] fileSystem = result.get().getFileSystem().split("\\/"); // Cela permet de récupérer le nom du bucket S3
                replayedScanner = scannerService.saveBucket(scanner, fileSystem[1]);
            }
        }
        return replayedScanner;
    }

    /**
     * Permet de comparer deux scans entre eux
     * 
     * @param id1 Identifiant du premier scan
     * @param id2 Identifiant du second scan
     * @return Message de comparaison
     */
    @GetMapping("/scanners/compare/{id1}/{id2}")
    public String compareScanners(@PathVariable long id1, @PathVariable long id2) {
        Optional<Scanner> result1 = scannerService.getScanner(id1);
        Optional<Scanner> result2 = scannerService.getScanner(id2);

        String message = "";

        if (!result1.isPresent() & !result2.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            Scanner scanner1 = result1.get();
            Scanner scanner2 = result2.get();

            // Informations sur les deux scans
            message = "Scanner" + scanner1.getId() + " vs. Scanner" + scanner2.getId() + " : \n";
            message += "StartFolder : " + scanner1.getStartFolder() + " vs. " + scanner2.getStartFolder() + "\n";
            message += "Date : " + scanner1.getDate() + " vs. " + scanner2.getDate() + "\n";
            message += "FilterName : " + scanner1.getFilterName() + " vs. " + scanner2.getFilterName() + "\n";
            message += "FilterType : " + scanner1.getFilterType() + " vs. " + scanner2.getFilterType() + "\n";
            message += "MaxFile : " + scanner1.getMaxFile() + " vs. " + scanner2.getMaxFile() + "\n";
            message += "MaxDepth : " + scanner1.getMaxDepth() + " vs. " + scanner2.getMaxDepth() + "\n";
            message += "Exectution Time : " + scanner1.getTime().toString() + "ms vs. " + scanner2.getTime().toString() + "ms\n";
            message += "FileSystem : " + scanner1.getFileSystem() + " vs. " + scanner2.getFileSystem();

            // On récupère les fichiers de chaque scan
            List<FileModel> files1 = scannerService.getFilesById(id1);
            List<FileModel> files2 = scannerService.getFilesById(id2);
            // On compare les deux listes de fichiers
            Map<String, List<FileModel>> compareFiles = Utils.compareElements(files1, files2);
            // Il y a soit les fichiers en commun, soit les fichiers qui ne sont que dans le scan 1, 
            // soit les fichiers qui ne sont que dans le scan 2
            List<FileModel> same = compareFiles.get("same");
            List<FileModel> diff1 = compareFiles.get("diff1");
            List<FileModel> diff2 = compareFiles.get("diff2");

            message += "\n\nSame Files : \n";
            message += same;

            message += "\n\nFiles only in Scanner" + id1 + " : \n";
            message += diff1;

            message += "\n\nFiles only in Scanner" + id2 + " : \n";
            message += diff2;
        }
        return message;
    }

    /**
     * Permet la création d'un scan en local selon différents filtres
     * 
     * @param scanner Scan d'initialisation
     * @param startFolder Chemin vers le dossier de départ du scan
     * @param filterName Nom du filtre sur les noms des fichiers
     * @param filterType Nom du filtre sur les extensions des fichiers
     * @param maxFile Nombre maximal de fichiers à sauvegarder
     * @param maxDepth Profondeur maximale dans l'arborescence des dossiers
     * @return Scan créé
     */
    @PostMapping("/scanners/local")
    public Scanner postScanner(
        @RequestBody Scanner scanner, 
        @RequestParam String startFolder, 
        @RequestParam(required = false) String filterName, 
        @RequestParam(required = false) String filterType, 
        @RequestParam(required = false) Integer maxFile, 
        @RequestParam(required = false) Integer maxDepth
    ) {
        scanner.setStartFolder(startFolder);
        scanner.setFilterName(filterName);
        scanner.setFilterType(filterType);
        scanner.setMaxFile(maxFile);
        scanner.setMaxDepth(maxDepth);

        scanner.setFileSystem("local");

        return scannerService.saveScanner(scanner, startFolder);
    }

    /**
     * Permet la création d'un scan sur AWS selon différents filtres
     * 
     * @param scanner Scan d'initialisation
     * @param startFolder Chemin vers le dossier de départ du scan
     * @param bucketName Non du bucket S3 à scanner
     * @param filterName Nom du filtre sur les noms des fichiers
     * @param filterType Nom du filtre sur les extensions des fichiers
     * @param maxFile Nombre maximal de fichiers à sauvegarder
     * @param maxDepth Profondeur maximale dans l'arborescence des dossiers
     * @return Scan créé
     */
    @PostMapping("/scanners/aws/{bucketName}")
    public Scanner postScannerAWS(
        @RequestBody Scanner scanner, 
        @RequestParam String startFolder, 
        @PathVariable String bucketName, 
        @RequestParam(required = false) String filterName, 
        @RequestParam(required = false) String filterType, 
        @RequestParam(required = false) Integer maxFile, 
        @RequestParam(required = false) Integer maxDepth
    ) {
        scanner.setStartFolder(startFolder);
        scanner.setFilterName(filterName);
        scanner.setFilterType(filterType);
        scanner.setMaxFile(maxFile);
        scanner.setMaxDepth(maxDepth);

        scanner.setFileSystem("aws/" + bucketName); // Cela permet de conserver le nom du bucket S3 et de le récupérer facilement

        return scannerService.saveBucket(scanner, bucketName);
    }

    /**
     * Permet de détruire un scan
     * 
     * @param id Identifiant du scan à détruire
     */
    @DeleteMapping("/scanners/{id}")
    public void deleteScanner(@PathVariable long id) {
        boolean found = scannerService.deleteScanner(id);
        if (!found) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Permet d'obtenir le temps d'exécution moyen des fichiers ou des dossiers
     * 
     * @param fileOdFolder Composant dont on souhaite connaitre le temps d'exécution. Soit file, soit folder
     * @return Message avec le temps de d'exécution
     * @throws Exception
     */
    @GetMapping("/scanners/stats/{fileOrFolder}")
    public String getAvgExecTime(@PathVariable String fileOrFolder) throws Exception {
        String message = "Le temps d'exécution moyen pour les ";
        if (fileOrFolder.equals("file")) {
            message += "fichiers est de ";
        } else if (fileOrFolder.equals("folder")) {
            message += "dossiers est de ";
        } else {
            throw new Exception("Mauvais paramètre. (file/folder)");
        }
        message += scannerService.avgExecTime(fileOrFolder);

        return message + "ms";
    }
    
    
}
