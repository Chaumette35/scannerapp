INSERT INTO scanner (start_folder, date, filterName, filterType, maxFile, maxDepth, exec_time)
VALUES ('/chemin/vers/dossier', '2024-02-16', 'filtre_nom', 'filtre_type', 100, 5, 120);

SET @last_scanner_id = LAST_INSERT_ID();

INSERT INTO folder (scanner_id, parent_id, name, type, size, maxDepth)
VALUES (@last_scanner_id, NULL, 'Dossier principal', 'dossier', 0, 3);

SET @last_folder_id = LAST_INSERT_ID();

INSERT INTO file (parent_id, name, type, size)
VALUES (@last_folder_id, 'fichier1.txt', 'texte', 1024),
       (@last_folder_id, 'fichier2.jpg', 'image', 2048);