DROP TABLE IF EXISTS scanner;
DROP TABLE IF EXISTS folder;
DROP TABLE IF EXISTS file;


CREATE TABLE scanner (
    scanner_id BIGINT PRIMARY KEY AUTO_INCREMENT,
    start_folder VARCHAR(255),
    date VARCHAR(255),
    filterName VARCHAR(255),
    filterType VARCHAR(255),
    maxFile INT,
    maxDepth INT,
    exec_time BIGINT, 
    fileSystem VARCHAR(255)
);

CREATE TABLE folder (
    folder_id BIGINT PRIMARY KEY AUTO_INCREMENT,
    scanner_id BIGINT NOT NULL,
    parent_id BIGINT,
    name VARCHAR(255),
    type VARCHAR(255),
    size BIGINT,
    exec_time BIGINT, 
    FOREIGN KEY (scanner_id) REFERENCES scanner(scanner_id),
    FOREIGN KEY (parent_id) REFERENCES folder(folder_id)
);

CREATE TABLE file (
    file_id BIGINT PRIMARY KEY AUTO_INCREMENT,
    parent_id BIGINT NOT NULL,
    name VARCHAR(255),
    type VARCHAR(255),
    size BIGINT,
    exec_time BIGINT, 
    FOREIGN KEY (parent_id) REFERENCES folder(folder_id)
);
