package fr.ensai.scanner.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.FolderModel;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Utils.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UtilsTest {

    @Test
    public void testSameElements() {
        // Given
        List<FileModel> filesTest1 = new ArrayList<>();

        FileModel file1 = new FileModel();
        file1.setName("test");
        FolderModel parentFolder1 = new FolderModel();
        parentFolder1.setName("test");
        file1.setFolder(parentFolder1);

        filesTest1.add(file1);

        List<FileModel> filesTest2 = new ArrayList<>();
        FileModel file2 = new FileModel();
        file2.setName("test");
        FolderModel parentFolder2 = new FolderModel();
        parentFolder2.setName("test");
        file2.setFolder(parentFolder2);

        filesTest2.add(file2);

        // When
        List<FileModel> resultTest = Utils.sameElements(filesTest1, filesTest2);

        // Then
        assertEquals(filesTest1, resultTest);
        assertEquals(1, resultTest.size());
    }

    @Test
    public void testDiffElements() {
        // Given
        List<FileModel> filesTest1 = new ArrayList<>();

        FileModel file1 = new FileModel();
        file1.setName("testDiff");
        FolderModel parentFolder1 = new FolderModel();
        parentFolder1.setName("test");
        file1.setFolder(parentFolder1);

        filesTest1.add(file1);

        List<FileModel> filesTest2 = new ArrayList<>();
        FileModel file2 = new FileModel();
        file2.setName("test");
        FolderModel parentFolder2 = new FolderModel();
        parentFolder2.setName("test");
        file2.setFolder(parentFolder2);

        filesTest2.add(file2);

        // When
        List<FileModel> resultTest = Utils.sameElements(filesTest1, filesTest2);

        // Then
        assertEquals(new ArrayList<>(), resultTest);
        assertEquals(0, resultTest.size());
    }

}
