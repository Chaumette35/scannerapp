package fr.ensai.scanner.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.scanner.model.FileModel;
import fr.ensai.scanner.model.FolderModel;
import fr.ensai.scanner.model.Scanner;


@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ScannerService.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScannerServiceTest {

    @Test
    public void testGetScanner() {
        Scanner scanner = new Scanner();
        scanner.setId(1L);
        scanner.setFilterName("test");

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.getScanner(1L)).thenReturn(Optional.of(scanner));

        Optional<Scanner> scannerFoundOpt = scannerServiceMock.getScanner(1L);

        assertTrue(scannerFoundOpt.isPresent());
        Scanner scannerFound = scannerFoundOpt.get();
        assertEquals("test", scannerFound.getFilterName());
    }

    @Test
    public void testGetScanners() {
        Scanner scanner1 = new Scanner();
        scanner1.setId(1L);
        scanner1.setFilterName("test1");

        Scanner scanner2 = new Scanner();
        scanner2.setId(2L);
        scanner2.setFilterName("test2");

        List<Scanner> scanners = new ArrayList<>();
        scanners.add(scanner1);
        scanners.add(scanner2);

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.getScanners()).thenReturn(scanners);

        List<Scanner> scannersFound = (List<Scanner>) scannerServiceMock.getScanners();

        assertEquals(2, scannersFound.size());
        assertEquals("test1", scannersFound.get(0).getFilterName());
        assertEquals("test2", scannersFound.get(1).getFilterName());
    }

    @Test
    public void testSaveScanner() {
        Scanner scanner = new Scanner();
        scanner.setFilterName("test");

        Scanner savedScanner = scanner;
        savedScanner.setId(1L);
        savedScanner.setStartFolder("testFolder");
        savedScanner.setFileSystem("local");

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.saveScanner(scanner, "testFolder")).thenReturn(savedScanner);

        Scanner scannerFound = scannerServiceMock.saveScanner(scanner, "testFolder");

        assertEquals(1L, scannerFound.getId());
        assertEquals("test", scannerFound.getFilterName());
        assertEquals("testFolder", scannerFound.getStartFolder());
        assertEquals("local", scannerFound.getFileSystem());
    }

    @Test
    public void testSaveBucket() {
        Scanner scanner = new Scanner();
        scanner.setFilterName("test");

        Scanner savedScanner = scanner;
        savedScanner.setId(1L);
        savedScanner.setStartFolder("testFolder");
        savedScanner.setFileSystem("aws/testBucket");

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.saveBucket(scanner, "testBucket")).thenReturn(savedScanner);

        Scanner scannerFound = scannerServiceMock.saveBucket(scanner, "testBucket");

        assertEquals(1L, scannerFound.getId());
        assertEquals("test", scannerFound.getFilterName());
        assertEquals("testFolder", scannerFound.getStartFolder());
        assertEquals("aws/testBucket", scannerFound.getFileSystem());
    }

    @Test
    public void testModifScannerTime() {
        Scanner scanner = new Scanner();

        Scanner scannerModifTime = scanner;
        scannerModifTime.setTime(200L);

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.modifScannerTime(scanner,200L)).thenReturn(scannerModifTime);

        Scanner scannerFound = scannerServiceMock.modifScannerTime(scanner, 200L);

        assertEquals(200L, scannerFound.getTime());
    }

    @Test
    public void testGetFilesById() {
        Scanner scanner = new Scanner();
        scanner.setId(1L);
        scanner.setStartFolder("testFolder");

        FolderModel startFolder = new FolderModel();
        startFolder.setId(1L);
        startFolder.setName("testFolder");

        FileModel file1 = new FileModel();
        file1.setFolder(startFolder);
        file1.setId(1L);
        file1.setName("test1");

        FileModel file2 = new FileModel();
        file2.setFolder(startFolder);
        file2.setId(2L);
        file2.setName("test2");

        FileModel file3 = new FileModel();
        file3.setFolder(startFolder);
        file3.setId(3L);
        file3.setName("test3");

        List<FileModel> files = new ArrayList<>();
        files.add(file1);
        files.add(file2);
        files.add(file3);

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.getFilesById(1L)).thenReturn(files);

        List<FileModel> filesFound = scannerServiceMock.getFilesById(1L);

        assertEquals(3, filesFound.size());
        assertEquals("testFolder", filesFound.get(0).getParentFolder().getName());
        assertEquals("test2", filesFound.get(1).getName());
        assertEquals(1, filesFound.get(0).getId());
    }

    @Test
    public void testDeleteScanner() {
        Scanner scanner = new Scanner();
        scanner.setId(1L);

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.deleteScanner(1L)).thenReturn(true);

        boolean result = scannerServiceMock.deleteScanner(1L);

        assertEquals(true, result);
    }

    @Test
    public void testAvgExecTimeGood() {
        String fileOrFolder = "file";

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.avgExecTime(fileOrFolder)).thenReturn(42.0);

        double result = scannerServiceMock.avgExecTime(fileOrFolder);

        assertEquals(42.0, result);
    }

    @Test
    public void testAvgExecTimeWrong() {
        String fileOrFolder = "test";

        ScannerService scannerServiceMock = mock(ScannerService.class);
        when(scannerServiceMock.avgExecTime(fileOrFolder)).thenReturn(null);

        Double result = scannerServiceMock.avgExecTime(fileOrFolder);

        assertEquals(null, result);
    }
}

