package fr.ensai.scanner.controller;

import static org.hamcrest.Matchers.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import fr.ensai.scanner.model.Scanner;
import fr.ensai.scanner.service.ScannerService;

@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ScannerController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScannerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ScannerService scannerService;

    @SuppressWarnings("null")
    @Test
    public void testGetScanners() throws Exception {
        // Mocking data
        List<Scanner> scanners = new ArrayList<>();
        scanners.add(new Scanner());
        scanners.add(new Scanner());

        when(scannerService.getScanners()).thenReturn(scanners);

        // Performing the GET request
        mvc.perform(MockMvcRequestBuilders.get("/scanners"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)));
    }

    @SuppressWarnings("null")
    @Test
    public void testDuplicateScanner() throws Exception {
        // Mocking data
        Scanner originalScanner = new Scanner();
        originalScanner.setId(1L);
        originalScanner.setStartFolder("/path/to/scan");
        originalScanner.setFileSystem("local");

        Scanner duplicatedScanner = originalScanner;
        duplicatedScanner.setId(originalScanner.getId() + 1);

        when(scannerService.getScanner(1L)).thenReturn(Optional.of(originalScanner));
        when(scannerService.saveScanner(any(), anyString())).thenReturn(duplicatedScanner);

        // Performing the GET request
        mvc.perform(MockMvcRequestBuilders.get("/scanners/duplicate/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(not(1))));
    }

    @SuppressWarnings("null")
    @Test
    public void testReplayScanner() throws Exception {
        // Mocking data
        Scanner originalScanner = new Scanner();
        originalScanner.setId(1L);
        originalScanner.setStartFolder("/path/to/scan");
        originalScanner.setFileSystem("local");

        Scanner replayedScanner = originalScanner;
        replayedScanner.setId(originalScanner.getId() + 1);

        when(scannerService.getScanner(1L)).thenReturn(Optional.of(originalScanner));
        when(scannerService.saveScanner(any(), anyString())).thenReturn(replayedScanner);

        // Performing the GET request
        mvc.perform(MockMvcRequestBuilders.get("/scanners/replay/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(not(1))));
    }

    @SuppressWarnings("null")
    @Test
    public void testCompareScanners() throws Exception {
        // Mocking data
        Scanner scanner1 = new Scanner();
        scanner1.setId(1L);
        scanner1.setStartFolder("/path/to/scan1");
        scanner1.setFileSystem("");
        scanner1.setTime(0L);
        Scanner scanner2 = new Scanner();
        scanner2.setId(2L);
        scanner2.setStartFolder("/path/to/scan2");
        scanner2.setFileSystem("");
        scanner2.setTime(0L);
        when(scannerService.getScanner(1L)).thenReturn(Optional.of(scanner1));
        when(scannerService.getScanner(2L)).thenReturn(Optional.of(scanner2));

        // Performing the GET request
        mvc.perform(MockMvcRequestBuilders.get("/scanners/compare/1/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("StartFolder : /path/to/scan1 vs. /path/to/scan2")));
    }

    @SuppressWarnings("null")
    @Test
    public void testPostScanner() throws Exception {
        // Mocking data
        Scanner scanner = new Scanner();
        scanner.setId(1L);
        scanner.setStartFolder("/path/to/scan");
        when(scannerService.saveScanner(any(), anyString())).thenReturn(scanner);

        // Performing the POST request
        String request = "{\"id\": 0, \"startFolder\": \"string\", \"date\": \"string\", ";
        request += "\"filterName\": \"string\", \"filterType\": \"string\",";
        request += " \"maxFile\": 0, \"maxDepth\": 0, \"time\": 0, \"fileSystem\": \"string\" }";

        mvc.perform(MockMvcRequestBuilders.post("/scanners/local?startFolder=" + scanner.getStartFolder())
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)));
    }

    @SuppressWarnings("null")
    @Test
    public void testPostScannerAWS() throws Exception {
        // Mocking data
        Scanner scanner = new Scanner();
        scanner.setId(1L);
        scanner.setStartFolder("/path/to/scan");
        when(scannerService.saveBucket(any(), anyString())).thenReturn(scanner);

        // Performing the POST request
        String request = "{\"id\": 0, \"startFolder\": \"string\", \"date\": \"string\", ";
        request += "\"filterName\": \"string\", \"filterType\": \"string\",";
        request += " \"maxFile\": 0, \"maxDepth\": 0, \"time\": 0, \"fileSystem\": \"string\" }";

        mvc.perform(MockMvcRequestBuilders.post("/scanners/aws/myBucket?startFolder=" + scanner.getStartFolder())
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)));
    }

    @Test
    public void testDeleteScanner() throws Exception {
        // Mocking data
        when(scannerService.deleteScanner(1L)).thenReturn(true);

        // Performing the DELETE request
        mvc.perform(MockMvcRequestBuilders.delete("/scanners/1"))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SuppressWarnings("null")
    @Test
    public void testGetAvgExecTime() throws Exception {
        // Mocking data
        when(scannerService.avgExecTime("file")).thenReturn(100.0);

        // Performing the GET request
        mvc.perform(MockMvcRequestBuilders.get("/scanners/stats/file"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.content().string(containsString("Le temps d'exécution moyen pour les fichiers est de 100.0ms")));
    }

}
