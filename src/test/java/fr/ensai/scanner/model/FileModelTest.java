package fr.ensai.scanner.model;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;


import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = FileModel.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FileModelTest {

    @Test
    public void testGettersAndSetters() {
        // Test each getter and setter with valid data
        FileModel file = new FileModel(5L, "fichier.pdf", "pdf", 2048);

        assertEquals(5L, file.getId());
        assertEquals("fichier.pdf", file.getName());
        assertEquals("pdf", file.getType());
        assertEquals(2048, file.getSize());

        // Try to set new values
        file.setName("nouveau.txt");
        file.setType("txt");
        file.setSize(1024);

        assertEquals("nouveau.txt", file.getName());
        assertEquals("txt", file.getType());
        assertEquals(1024, file.getSize());
    }

    @Test
    public void testEqualsFiles() {
        FileModel file1 = new FileModel();
        file1.setName("test");
        FolderModel parentFolder1 = new FolderModel();
        parentFolder1.setName("parent");
        file1.setFolder(parentFolder1);

        FileModel file2 = new FileModel();
        file2.setName("test");
        FolderModel parentFolder2 = new FolderModel();
        parentFolder2.setName("parent");
        file2.setFolder(parentFolder2);


        assertTrue(file1.equals(file2));
    }

    @Test
    public void testNotEqualsFileName() {
        FileModel file1 = new FileModel();
        file1.setName("test");
        FolderModel parentFolder1 = new FolderModel();
        parentFolder1.setName("parent");
        file1.setFolder(parentFolder1);

        FileModel file2 = new FileModel();
        file2.setName("testDiff");
        FolderModel parentFolder2 = new FolderModel();
        parentFolder2.setName("parent");
        file2.setFolder(parentFolder2);


        assertFalse(file1.equals(file2));
    }

    @Test
    public void testNotEqualsParentName() {
        FileModel file1 = new FileModel();
        file1.setName("test");
        FolderModel parentFolder1 = new FolderModel();
        parentFolder1.setName("parent");
        file1.setFolder(parentFolder1);

        FileModel file2 = new FileModel();
        file2.setName("test");
        FolderModel parentFolder2 = new FolderModel();
        parentFolder2.setName("parentDiff");
        file2.setFolder(parentFolder2);


        assertFalse(file1.equals(file2));
    }

}

