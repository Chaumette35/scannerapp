package fr.ensai.scanner.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Scanner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScannerTest {

    @Test
    public void testGettersAndSetters() {
        // Test each getter and setter with valid data
        Scanner scanner = new Scanner(5L, 300L);

        assertEquals(5L, scanner.getId());
        assertEquals(300L, scanner.getTime());

        // Try to set new values
        scanner.setFileSystem("local");
        scanner.setFilterName("test");
        scanner.setFilterType("pdf");
        scanner.setMaxFile(3);
        scanner.setMaxDepth(5);

        assertEquals("local", scanner.getFileSystem());
        assertEquals("test", scanner.getFilterName());
        assertEquals("pdf", scanner.getFilterType());
        assertEquals(3, scanner.getMaxFile());
        assertEquals(5, scanner.getMaxDepth());
    }
    
}
