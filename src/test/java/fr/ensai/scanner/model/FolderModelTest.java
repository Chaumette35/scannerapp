package fr.ensai.scanner.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FolderModel.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FolderModelTest {

    @Test
    public void testGettersAndSetters() {
        // Test each getter and setter with valid data
        FolderModel folder = new FolderModel(5L, "Téléchargement", "Local folder", 2048L);

        assertEquals(5L, folder.getId());
        assertEquals("Téléchargement", folder.getName());
        assertEquals("Local folder", folder.getType());
        assertEquals(2048, folder.getSize());

        // Try to set new values
        folder.setName("Download");
        folder.setType("AWS folder");
        folder.setSize(1024L);

        assertEquals("Download", folder.getName());
        assertEquals("AWS folder", folder.getType());
        assertEquals(1024L, folder.getSize());
    }

}
