package fr.ensai.scanner;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@SpringBootTest(classes = StartApplication.class)
public class ApplicationTests {

    @Test
	void contextLoads() {
	}
    
}
